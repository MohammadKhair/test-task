package com.test.task.ui.department.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.test.task.common.App
import com.test.task.data.models.DepartmentModel
import com.test.task.databinding.AddDepartmentFragmentBinding
import com.test.task.ui.department.DepartmentViewModel
import com.test.task.utils.extensions.random
import java.text.SimpleDateFormat
import java.util.*

/**
 * this fragment responsible of add new departments
 *
 */

class AddDepartment : Fragment() {

    private lateinit var binding: AddDepartmentFragmentBinding
    private lateinit var viewModel: DepartmentViewModel
    private val model= DepartmentModel(id = 0, name = "", createdDate = "", updateDate = "")

    private val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= AddDepartmentFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(DepartmentViewModel::class.java)
        App.instance.getComponent().injectDepartmentViewModel(viewModel = viewModel)
        model.apply {
            setCreatedDate(createdDate = sdf.format(Date()))
            setUpdateDate(updateDate = sdf.format(Date()))
            setId(id = random(start = 1, end = 1000))
        }
        binding.let { it.viewModel= viewModel; it.view= this; it.model= model }

    }
}
