package com.test.task.ui.signIn

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.test.task.data.models.SignInModel
import com.test.task.databinding.SignInFragmentBinding
import com.test.task.utils.extensions.log
import com.test.task.utils.extensions.toast

class SignInFragment : Fragment() {

    private lateinit var binding: SignInFragmentBinding
    private lateinit var viewModel: SignInViewModel
    val model = SignInModel("", "")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SignInFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(SignInViewModel::class.java)
        binding.let { it.view = this; it.viewModel = viewModel; it.model = model }

       viewModel.configureGoogleSignIn(context = requireContext())

        binding.googleButton.setOnClickListener { viewModel.signViaGoogle(view = this) }
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == googleCode) {
                val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    viewModel.firebaseAuthWithGoogle(acct = account!!, context = requireContext())

                } catch (e: ApiException) {
                    viewModel.isLoading.set(false)
                    log("googleSignFail111111", "$task,,,,"+ task.exception)
                    "Google sign in failed:( $task".toast(context = requireContext())
                }
            }

        } else {
            viewModel.isLoading.set(false)
            "${Activity.RESULT_OK}, $requestCode".toast(context = requireContext())
            log("googleSignFail22222", Activity.RESULT_OK.toString())

        }
    }

    companion object {
        const val googleCode: Int = 1001
    }
}
