package com.test.task.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.test.task.R
import com.test.task.data.services.ClearPreferenceService
import com.test.task.ui.base.BaseActivity
import com.test.task.utils.extensions.startAffinity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * I need this activity to setup the ui navigation component (home_nav) and using it with bottom navigation view
 *
 */

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        instant= this
        setUpNavigation()
    }

    private fun setUpNavigation() {
        val navController = Navigation.findNavController(this, R.id.home_nav_host_fragment)
        NavigationUI.setupWithNavController(bottomNavigationView, navController)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    /**
     * this function to control the Intents from this activity to any other activity without going back
     */
    fun navigate(to: AppCompatActivity) {
        startAffinity(context = this, to = to)
    }

    /**
     * this function used to stop the service
     */
    fun stopService() {
        stopService(Intent(this, ClearPreferenceService::class.java))
    }

    override fun onStop() {
        /**
         * this intent to start the service that responsible of starting the counter to clear the login info
         */
        startService(Intent(this, ClearPreferenceService::class.java))

        super.onStop()
    }

    companion object {
        lateinit var instant: MainActivity
    }
}
