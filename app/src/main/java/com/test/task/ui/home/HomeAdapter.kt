package com.test.task.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.test.task.R
import com.test.task.data.di.annotations.EmployeesAnno
import com.test.task.data.di.scope.HomeScope
import com.test.task.data.models.DepartmentModel
import com.test.task.data.models.EmployeesModel
import com.test.task.databinding.EmployeeItemBinding
import javax.inject.Inject

/**
 * this adapter responsible of showing the all employees in the Home fragment
 *
 */

@HomeScope class HomeAdapter @Inject constructor(@EmployeesAnno val modelList: List<EmployeesModel>, private val navController: NavController) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val employeeItemBinding = EmployeeItemBinding.inflate(layoutInflater, parent, false)

        return ViewHolder(employeeItemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
            = holder.bind(modelList[position])

    override fun getItemCount(): Int = modelList.size

    inner class ViewHolder(private var employeeItemBinding: EmployeeItemBinding) :
        RecyclerView.ViewHolder(employeeItemBinding.root) {

        fun bind(model: EmployeesModel) {
            employeeItemBinding.model = model
            employeeItemBinding.navigate= this

            employeeItemBinding.executePendingBindings()
        }

        fun navigates(model: EmployeesModel, to: String) {
            when (to) {
                "employee" -> {
                    val bundle = bundleOf("employee" to model)
                    navController.navigate(R.id.action_home_to_editEmployee, bundle)

                }
                "image" -> {
                    val bundle = bundleOf("employee" to model)
                    navController.navigate(R.id.action_home_to_editEmployeeImage, bundle)

                }
                "department" -> {
                    val bundle = bundleOf("department" to DepartmentModel(id = model.getDepartmentId(), name = model.getDepartmentName(), createdDate = model.getCreatedDate(), updateDate = ""))
                    navController.navigate(R.id.action_home_to_editDepartment, bundle)

                }
            }
        }
    }
}
