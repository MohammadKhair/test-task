package com.test.task.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.firebase.auth.FirebaseAuth
import com.test.task.R
import com.test.task.data.maneger.CacheManager
import com.test.task.databinding.SettingsFragmentBinding
import com.test.task.ui.main.MainActivity
import com.test.task.ui.splash.SplashActivity
import com.test.task.utils.extensions.startAffinity

class SettingsFragment : Fragment() {

    private lateinit var binding: SettingsFragmentBinding
    private  lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= SettingsFragmentBinding.inflate( inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        binding.let { it.view= this; it.navController= navController }

    }

    /**
     * this function to to control the navigating and I'm using it from xml using the data binding
     */
    fun navigate(navController: NavController, to: String) {
        when (to) {
            "language" -> navController.navigate(R.id.action_settings_to_language2)
            "department" -> navController.navigate(R.id.action_settings_to_addDepartment)
            "employee" -> navController.navigate(R.id.action_settings_to_addEmployee)
        }
    }

    fun signOut() {
        FirebaseAuth.getInstance().signOut()
        CacheManager.instance.clearAll()
        /**
         * this function used to stop the service
         */
       MainActivity.instant.stopService()

        startAffinity(context = MainActivity.instant, to = SplashActivity())
    }
}