package com.test.task.ui.signIn

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Handler
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.test.task.R
import com.test.task.data.maneger.CacheManager
import com.test.task.data.models.SignInModel
import com.test.task.ui.main.MainActivity
import com.test.task.ui.signIn.SignInFragment.Companion.googleCode
import com.test.task.ui.splash.SplashActivity
import com.test.task.utils.extensions.getSHA512Hash
import com.test.task.utils.extensions.log
import com.test.task.utils.extensions.toast

class SignInViewModel(application: Application) : AndroidViewModel(application) {

    val isLoading = ObservableField(false)
    val isUsername = ObservableField(false)
    val isPassword = ObservableField(false)

    private val firebaseAuth: FirebaseAuth= FirebaseAuth.getInstance()
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mGoogleSignInOptions: GoogleSignInOptions

    fun signIn(context: Context, model: SignInModel) {
        isLoading.set(true)
        when {
            model.getUsername().isEmpty()-> { isLoading.set(false); isUsername.set(true) }
            model.getPassword().isEmpty()-> { isLoading.set(false); isPassword.set(true) }

            else -> {
                if (model.getUsername()== "admin" && model.getPassword()== "123456") {
                    Handler().postDelayed({
                        CacheManager.instance.apply {
                            setUsername(username = model.getUsername())
                            setPassword(password = getSHA512Hash(password = model.getPassword()))
                        }

                        SplashActivity.instant.navigate(to = MainActivity())

                    },2000)// just for testing

                } else {
                    Handler().postDelayed({ isLoading.set(false); "Username or Password incorrect".toast(context= context) },2000)// just for testing
                }
            }
        }
    }

    fun configureGoogleSignIn(context: Context) {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(context, mGoogleSignInOptions)
    }

    fun signViaGoogle(view: SignInFragment) {
        isLoading.set(true)

        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        view.startActivityForResult(signInIntent, googleCode)
    }

    fun firebaseAuthWithGoogle(acct: GoogleSignInAccount, context: Context) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                isLoading.set(false)
                val user= FirebaseAuth.getInstance().currentUser

                CacheManager.instance.setUsername(username = user!!.displayName!!)
                SplashActivity.instant.navigate(to = MainActivity())

            } else {
                isLoading.set(false)
                log("googleSignFail", it.toString())
                "Google sign in failed:( $it".toast(context = context)
            }
        }
    }
}
//SHA certificate fingerprints
