package com.test.task.ui.splash

import androidx.appcompat.app.AppCompatActivity

interface SplashContract {
    fun navigate(to: AppCompatActivity)
}