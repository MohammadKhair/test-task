package com.test.task.ui.base

import com.akexorcist.localizationactivity.ui.LocalizationActivity

/**
 * this BaseActivity extend LocalizationActivity which control the language in the app
 * we extend it from all activities
 */
open class BaseActivity : LocalizationActivity()
