package com.test.task.ui.employee

import android.app.Application
import android.content.Context
import android.os.Handler
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.test.task.R
import com.test.task.common.App
import com.test.task.data.models.DepartmentModel
import com.test.task.data.models.EmployeesModel
import com.test.task.data.repositories.EmployeesRepository
import com.test.task.ui.employee.add.AddEmployee
import com.test.task.ui.employee.edit.EditEmployee
import com.test.task.ui.employee.edit.EditEmployeeImage
import com.test.task.utils.extensions.convertToString
import com.test.task.utils.extensions.log
import com.test.task.utils.extensions.plusAssign
import com.test.task.utils.extensions.toast
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * this viewModel handling with observing from EmployeesRepository and to control the coming data before sending it to the view(activity, fragment)
 *
 */

class EmployeeViewModel(application: Application) : AndroidViewModel(application) {

    @Inject lateinit var repository: EmployeesRepository

    val isLoading = ObservableField(false)
    val isFirstName = ObservableField(false)
    val isLastName = ObservableField(false)
    val isEmail = ObservableField(false)
    val isMobileNumber = ObservableField(false)
    val isGender = ObservableField(false)
    val isAddress = ObservableField(false)
    val isPassword = ObservableField(false)

    var model = MutableLiveData<List<DepartmentModel>>()

    private val compositeDisposable = CompositeDisposable()

    fun getLocalDepartment() {
        App.instance.getComponent().injectEmployeeViewModel(viewModel= this)

        isLoading.set(true)

        Handler().postDelayed({
            compositeDisposable += repository
                .getLocalDepartment().subscribe({
                    isLoading.set(false)
                    model.value= it
                }, {
                    isLoading.set(false)

                })
        },2000)// just for testing
    }

    fun addEmployee(context: Context, model: EmployeesModel, view: AddEmployee) {
        isLoading.set(true)

        Handler().postDelayed({
            when {
                model.getFirstName().isEmpty() -> {isLoading.set(false); isFirstName.set(true) }
                model.getDepartmentName().isEmpty() -> {isLoading.set(false); context.getString(R.string.department_name_error2).toast(context= context) }
                model.getFirstName().isEmpty() -> {isLoading.set(false); isFirstName.set(true) }
                model.getLastName().isEmpty() -> {isLoading.set(false); isLastName.set(true) }
                model.getEmail().isEmpty() -> {isLoading.set(false); isEmail.set(true) }
                model.getMobileNumber().isEmpty() -> {isLoading.set(false); isMobileNumber.set(true) }
                model.getGender().isEmpty() -> {isLoading.set(false); isGender.set(true) }
                model.getAddress().isEmpty() -> {isLoading.set(false); isAddress.set(true) }
                model.getPassword().isEmpty() -> {isLoading.set(false); isPassword.set(true) }

                else -> {
                    compositeDisposable += repository
                        .insertLocalEmployee(model = model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableCompletableObserver() {

                            override fun onError(e: Throwable) {
                                isLoading.set(false)
                                context.getString(R.string.add_employee_error).toast(context = context)
                                log("error", e.message.toString())
                            }

                            override fun onComplete() {
                                isLoading.set(false)
                                context.getString(R.string.add_employee_success).toast(context = context)
                                view.requireActivity().onBackPressed()

                            }
                        })
                }
            }
        },2000)// just for testing
    }

    fun pickImage(view: EditEmployeeImage) {
        PickImageDialog.build(PickSetup())
            .setOnPickResult { r ->
                view.binding.image.setImageBitmap(r.bitmap)
                view.model.setPhoto(convertToString(r.bitmap))

            }

            .setOnPickCancel {
                "Cancel".toast(view.requireContext())
            }

            .show(view.requireActivity().supportFragmentManager)
    }

    fun updateImage(context: Context, model: EmployeesModel, view: EditEmployeeImage) {
        App.instance.getComponent().injectEmployeeViewModel(viewModel= this)

        isLoading.set(true)
        Handler().postDelayed({
            when {
                model.getPhoto().isEmpty() -> {isLoading.set(false); context.getString(R.string.employee_image_error).toast(context= context) }

                else -> {
                    compositeDisposable += repository
                        .updateLocalEmployeeImage(model = model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableCompletableObserver() {

                            override fun onError(e: Throwable) {
                                isLoading.set(false)
                                context.getString(R.string.add_employee_error).toast(context = context)
                                log("error", e.message.toString())
                            }

                            override fun onComplete() {
                                isLoading.set(false)
                                context.getString(R.string.add_employee_success).toast(context = context)
                                view.requireActivity().onBackPressed()

                            }
                        })
                }
            }
        },2000)// just for testing
    }

    fun updateEmployee(context: Context, model: EmployeesModel, view: EditEmployee) {
        isLoading.set(true)

        Handler().postDelayed({
            when {
                model.getFirstName().isEmpty() -> {isLoading.set(false); isFirstName.set(true) }
                model.getDepartmentName().isEmpty() -> {isLoading.set(false); context.getString(R.string.department_name_error2).toast(context= context) }
                model.getFirstName().isEmpty() -> {isLoading.set(false); isFirstName.set(true) }
                model.getLastName().isEmpty() -> {isLoading.set(false); isLastName.set(true) }
                model.getEmail().isEmpty() -> {isLoading.set(false); isEmail.set(true) }
                model.getMobileNumber().isEmpty() -> {isLoading.set(false); isMobileNumber.set(true) }
                model.getGender().isEmpty() -> {isLoading.set(false); isGender.set(true) }
                model.getAddress().isEmpty() -> {isLoading.set(false); isAddress.set(true) }
                model.getPassword().isEmpty() -> {isLoading.set(false); isPassword.set(true) }

                else -> {
                    compositeDisposable += repository
                        .updateLocalEmployee(model = model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableCompletableObserver() {

                            override fun onError(e: Throwable) {
                                isLoading.set(false)
                                context.getString(R.string.add_employee_error).toast(context = context)
                                log("error", e.message.toString())
                            }

                            override fun onComplete() {
                                isLoading.set(false)
                                context.getString(R.string.add_employee_success).toast(context = context)
                                view.requireActivity().onBackPressed()

                            }
                        })
                }
            }
        },2000)// just for testing
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}
