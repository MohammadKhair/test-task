package com.test.task.ui.splash

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavController
import com.google.firebase.auth.FirebaseAuth
import com.test.task.R
import com.test.task.data.maneger.CacheManager
import com.test.task.ui.main.MainActivity

class SplashViewModel(application: Application) : AndroidViewModel(application) {
    fun navigate(navController: NavController) {
        if (CacheManager.instance.getUsername()!= "" || FirebaseAuth.getInstance().currentUser!= null) {
            SplashActivity.instant.navigate(to = MainActivity())

        } else {
            navController.navigate(R.id.action_splash_to_signInFragment)
        }

    }
}
