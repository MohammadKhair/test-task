package com.test.task.ui.employee.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.test.task.data.models.EmployeesModel
import com.test.task.databinding.FragmentEditEmployeeImageBinding
import com.test.task.ui.employee.EmployeeViewModel
import com.test.task.ui.employee.add.AddEmployee
import java.text.SimpleDateFormat
import java.util.*

/**
 * this fragment responsible of add edit employees picture
 *
 */

class EditEmployeeImage : Fragment() {

    lateinit var binding: FragmentEditEmployeeImageBinding
    private lateinit var viewModel: EmployeeViewModel
    lateinit var model: EmployeesModel

    private val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentEditEmployeeImageBinding.inflate(inflater, container, false)
        model = requireArguments().getParcelable("employee")!!
        AddEmployee.model.setUpdateDate(updateDate = sdf.format(Date()))

        viewModel = ViewModelProvider(this).get(EmployeeViewModel::class.java)
        binding.let { it.viewModel= viewModel; it.view= this; it.model= model }

        return binding.root
    }
}
