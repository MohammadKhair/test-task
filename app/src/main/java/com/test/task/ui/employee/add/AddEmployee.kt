package com.test.task.ui.employee.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.test.task.common.App
import com.test.task.data.models.EmployeesModel
import com.test.task.databinding.AddEmployeeFragmentBinding
import com.test.task.ui.employee.DepartmentsAdapter
import com.test.task.ui.employee.EmployeeViewModel
import com.test.task.utils.extensions.random
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * this fragment responsible of add employees
 *
 */

class AddEmployee : Fragment() {

    private lateinit var binding: AddEmployeeFragmentBinding
    private lateinit var viewModel: EmployeeViewModel
    @Inject lateinit var adapter: DepartmentsAdapter

    private val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= AddEmployeeFragmentBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this).get(EmployeeViewModel::class.java)
        viewModel.getLocalDepartment()

        model.apply {
            setCreatedDate(createdDate = sdf.format(Date()))
            setUpdateDate(updateDate = sdf.format(Date()))
            setId(id = random(start = 1, end = 1000))
        }

        binding.let { it.viewModel= viewModel; it.view= this; it.model= model }

        returnEmployee()

        return binding.root
    }

    private fun returnEmployee() {
        viewModel.model.observe(requireActivity(), Observer { it?.let {
            App.instance.getComponent().getAddEmployeeComponentBuilder.list(model = it).build().injectFragment(addEmployee = this)

            binding.departmentRv.adapter = adapter
        }
        })
    }

    companion object {
        var model= EmployeesModel(id = 0, departmentId = 0, departmentName = "", createdDate = "", updateDate = "", firstName = "", lastName = "", email = "", mobileNumber = "", password = "", gender = "", address = "", photo = "")

    }
}
