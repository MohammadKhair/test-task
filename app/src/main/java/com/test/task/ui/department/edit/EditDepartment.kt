package com.test.task.ui.department.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.test.task.common.App
import com.test.task.data.models.DepartmentModel
import com.test.task.databinding.EditDepartmentFragmentBinding
import com.test.task.ui.department.DepartmentViewModel
import java.text.SimpleDateFormat
import java.util.*

/**
 * this fragment responsible of add edit departments
 *
 */

class EditDepartment : Fragment() {

    private lateinit var binding: EditDepartmentFragmentBinding
    private lateinit var viewModel: DepartmentViewModel
    private lateinit var model: DepartmentModel
    private val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= EditDepartmentFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(DepartmentViewModel::class.java)
        App.instance.getComponent().injectDepartmentViewModel(viewModel = viewModel)

        model = requireArguments().getParcelable("department")!!
        model.setUpdateDate(updateDate = sdf.format(Date()))
        binding.let { it.viewModel= viewModel; it.view= this; it.model= model }

    }
}