package com.test.task.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.task.R
import com.test.task.data.services.ClearPreferenceService
import com.test.task.ui.base.BaseActivity
import com.test.task.utils.extensions.startAffinity

class SplashActivity : BaseActivity(), SplashContract {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        instant= this

        stopService(Intent(this, ClearPreferenceService::class.java))
    }

    /**
     * this function to to control the Intents from this activity to any other activity without going back
     */
    override fun navigate(to: AppCompatActivity) {
        startAffinity(context = this, to = to)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    companion object {

        lateinit var instant: SplashActivity
    }
}
