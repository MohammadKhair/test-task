package com.test.task.ui.home

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.test.task.data.models.EmployeesModel
import com.test.task.data.repositories.HomeRepository
import com.test.task.utils.extensions.plusAssign
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * this viewModel handling with observing from HomeRepository and to control the coming data before sending it to the view(activity, fragment)
 *
 */

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    @Inject lateinit var repository: HomeRepository

    val isLoading = ObservableField(false)

    var model = MutableLiveData<List<EmployeesModel>>()

    private val compositeDisposable = CompositeDisposable()

    fun loadEmployees() {
        isLoading.set(true)

        compositeDisposable += repository.getEmployees().subscribe({
            isLoading.set(false)
            model.value= it
        }, {
            isLoading.set(false)

        })
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}
