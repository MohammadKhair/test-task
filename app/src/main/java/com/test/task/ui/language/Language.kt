package com.test.task.ui.language

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.akexorcist.localizationactivity.core.LanguageSetting
import com.test.task.R
import com.test.task.databinding.FragmentLanguageBinding
import com.test.task.ui.main.MainActivity
import com.test.task.ui.splash.SplashActivity
import com.test.task.utils.extensions.startAffinity
import com.test.task.utils.extensions.toast
import java.util.*

/**
 * this fragment handling the language
 *
 */

class Language : Fragment() {

    private lateinit var binding: FragmentLanguageBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentLanguageBinding.inflate(inflater, container, false)
        binding.view= this

        return binding.root
    }

    fun switchLanguage(context: Context) {
        if (Locale.getDefault().language == "en") {
            LanguageSetting.setLanguage(context, Locale("ar"))
            context.getString(R.string.ar_lan).toast(context)

        } else {
            LanguageSetting.setLanguage(context, Locale("en"))
            context.getString(R.string.en_lan).toast(context)

        }

        startAffinity(context = MainActivity.instant, to = SplashActivity())
    }
}