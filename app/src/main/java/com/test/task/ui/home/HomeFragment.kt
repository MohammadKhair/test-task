package com.test.task.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.test.task.common.App
import com.test.task.data.di.component.HomeComponent
import com.test.task.databinding.HomeFragmentBinding
import javax.inject.Inject

class HomeFragment : Fragment() {

    private lateinit var binding: HomeFragmentBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var navController: NavController
    private lateinit var component: HomeComponent
    @Inject lateinit var adapter: HomeAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= HomeFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.viewModel= viewModel
        binding.executePendingBindings()

        App.instance.getComponent().injectHomeViewModel(homeViewModel = viewModel)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        returnEmployees()
    }

    private fun returnEmployees() {
        viewModel.apply {
            loadEmployees()
            model.observe(requireActivity(), Observer { it?.let {
                component= App.instance.getComponent().getHomeComponentBuilder.list(model = it).navController(navController = navController).build()
                component.injectHomeFragment(homeFragment = this@HomeFragment)

                binding.employeesRv.adapter= adapter
            } })
        }
    }
}
