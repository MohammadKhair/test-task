package com.test.task.ui.employee.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.test.task.common.App
import com.test.task.data.models.EmployeesModel
import com.test.task.databinding.EditEmployeeFragmentBinding
import com.test.task.ui.employee.DepartmentsAdapter
import com.test.task.ui.employee.EmployeeViewModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * this fragment responsible of edit employees
 *
 */

class EditEmployee : Fragment() {

    private lateinit var binding: EditEmployeeFragmentBinding
    private lateinit var viewModel: EmployeeViewModel
    @Inject lateinit var adapter: DepartmentsAdapter

    private val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= EditEmployeeFragmentBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this).get(EmployeeViewModel::class.java)
        viewModel.getLocalDepartment()

        model = requireArguments().getParcelable("employee")!!

        model.apply { setUpdateDate(updateDate = sdf.format(Date())) }

        binding.let { it.viewModel= viewModel; it.view= this; it.model= model }

        returnEmployee()

        return binding.root
    }

    private fun returnEmployee() {
        viewModel.model.observe(requireActivity(), Observer { it?.let {
            App.instance.getComponent().getEditEmployeeComponentBuilder.list(model = it).build().injectFragment(editEmployee = this)

            binding.departmentRv.adapter = adapter
        }
        })
    }

    companion object {
        lateinit var model: EmployeesModel

    }
}