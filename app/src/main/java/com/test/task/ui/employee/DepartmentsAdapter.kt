package com.test.task.ui.employee

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.task.data.di.annotations.DepartmentsAnno
import com.test.task.data.di.scope.AddEmployeeScope
import com.test.task.data.models.DepartmentModel
import com.test.task.databinding.DepartmentItemBinding
import com.test.task.ui.employee.add.AddEmployee
import kotlinx.android.synthetic.main.department_item.view.*
import javax.inject.Inject

/**
 * this adapter responsible of showing the department name in the AddEmployee fragment
 *
 */

@AddEmployeeScope class DepartmentsAdapter @Inject constructor(@DepartmentsAnno val modelList: List<DepartmentModel>) :
    RecyclerView.Adapter<DepartmentsAdapter.ViewHolder>() {

    // if selected = -1, there is no default selection
    // if selected = 0, 1st item is selected by default
    var selected= -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val departmentItemBinding = DepartmentItemBinding.inflate(layoutInflater, parent, false)

        return ViewHolder(departmentItemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
            = holder.bind(modelList[position])

    override fun getItemCount(): Int = modelList.size

    inner class ViewHolder(private var departmentItemBinding: DepartmentItemBinding) :
        RecyclerView.ViewHolder(departmentItemBinding.root) {

        fun bind(model: DepartmentModel) {
            departmentItemBinding.model = model

            departmentItemBinding.root.name.isChecked = selected== adapterPosition

            departmentItemBinding.root.name.setOnClickListener {

                if (selected != adapterPosition) {
                    notifyItemChanged(selected)
                    selected = adapterPosition
                    AddEmployee.model.apply {
                        setDepartmentId(model.getId())
                        setDepartmentName(model.getName())
                    }
                }
            }

            departmentItemBinding.executePendingBindings()
        }
    }
}
