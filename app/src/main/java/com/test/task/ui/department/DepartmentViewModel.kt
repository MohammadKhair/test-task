package com.test.task.ui.department

import android.app.Application
import android.content.Context
import android.os.Handler
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.test.task.R
import com.test.task.data.models.DepartmentModel
import com.test.task.data.repositories.DepartmentRepository
import com.test.task.ui.department.add.AddDepartment
import com.test.task.ui.department.edit.EditDepartment
import com.test.task.utils.extensions.log
import com.test.task.utils.extensions.plusAssign
import com.test.task.utils.extensions.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * this viewModel handling with observing from DepartmentRepository and to control the coming data before sending it to the view(activity, fragment)
 *
 */

class DepartmentViewModel(application: Application) : AndroidViewModel(application) {

    @Inject lateinit var repository: DepartmentRepository

    val isLoading = ObservableField(false)
    val isName = ObservableField(false)

    private val compositeDisposable = CompositeDisposable()

    fun addDepartment(context: Context, model: DepartmentModel, view: AddDepartment) {
        isLoading.set(true)
        when {
            model.getName().isEmpty()-> { isLoading.set(false); isName.set(true) }

            else -> {
                Handler().postDelayed({
                    repository.saveLocalDepartment(department = model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableCompletableObserver() {

                            override fun onError(e: Throwable) {
                                isLoading.set(false)
                                context.getString(R.string.add_department_error).toast(context = context)
                                log("error", e.message.toString())
                            }

                            override fun onComplete() {
                                isLoading.set(false)
                                context.getString(R.string.add_department_success).toast(context = context)
                                view.requireActivity().onBackPressed()

                            }
                        })
                },2000)// just for testing
            }
        }
    }

    fun updateDepartment(context: Context, model: DepartmentModel, view: EditDepartment) {
        isLoading.set(true)
        when {
            model.getName().isEmpty()-> { isLoading.set(false); isName.set(true) }

            else -> {
                Handler().postDelayed({
                    compositeDisposable += repository
                        .updateLocalDepartment(model = model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableCompletableObserver() {

                            override fun onError(e: Throwable) {
                                isLoading.set(false)
                                context.getString(R.string.add_department_error).toast(context = context)
                                log("error", e.message.toString())
                            }

                            override fun onComplete() {
                                isLoading.set(false)
                                context.getString(R.string.add_department_success).toast(context = context)
                                view.requireActivity().onBackPressed()

                            }
                        })
                },2000)// just for testing
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}
