package com.test.task.data.dao

import androidx.room.*
import com.test.task.data.models.DepartmentModel
import com.test.task.data.models.EmployeesModel
import io.reactivex.Completable
import io.reactivex.Observable

/**
 * this Dao to control the some func in employee table in the SQLite database
 */

@Dao
interface EmployeesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEmployees(employee: EmployeesModel) : Completable

    @Query("SELECT e.${EmployeesModel.employeeId}, e.${EmployeesModel.employeeDepartmentId}, d.${DepartmentModel.departmentName}, d.${DepartmentModel.departmentCreatedDate}, d.${DepartmentModel.departmentUpdateDate}, e.${EmployeesModel.employeeCreatedDate}, e.${EmployeesModel.employeeUpdateDate}, e.${EmployeesModel.employeeFirstName}, e.${EmployeesModel.employeeLastName}, e.${EmployeesModel.employeeEmail}, e.${EmployeesModel.employeeMobileNumber}, e.${EmployeesModel.employeePassword}, e.${EmployeesModel.employeeGender}, e.${EmployeesModel.employeeAddress}, e.${EmployeesModel.employeePhoto} FROM ${EmployeesModel.tableName} e INNER JOIN ${DepartmentModel.tableName} d WHERE e.${EmployeesModel.employeeDepartmentId}= d.${DepartmentModel.departmentId}")
    fun getEmployees(): Observable<List<EmployeesModel>>

    @Update
    fun updateEmployeeImage(model: EmployeesModel) : Completable

    @Update
    fun updateLocalEmployee(model: EmployeesModel) : Completable
}
