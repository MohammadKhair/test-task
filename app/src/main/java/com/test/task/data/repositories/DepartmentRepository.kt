package com.test.task.data.repositories

import com.test.task.common.App
import com.test.task.data.db.DataBase
import com.test.task.data.models.DepartmentModel
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * this repository is the responsible for hit on the apis and take the data and save it in local database
 * but there is no online apis now so it's just handling with local database (roomDB)
 *
 */

class DepartmentRepository @Inject constructor() {
    fun saveLocalDepartment(department: DepartmentModel): Completable =
        DataBase.getDatabaseInstance(App.instance).departmentDao()
            .insertDepartment(department= department)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

//    fun updateLocalDepartment(model: DepartmentModel): Completable =
//        DataBase.getDatabaseInstance(App.instance).departmentDao()
//            .updateDepartment(id = model.getId(), name = model.getName(), updateDate = model.getUpdateDate())
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())

    fun updateLocalDepartment(model: DepartmentModel): Completable =
        DataBase.getDatabaseInstance(App.instance).departmentDao()
            .updateDepartment(model = model)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

}
