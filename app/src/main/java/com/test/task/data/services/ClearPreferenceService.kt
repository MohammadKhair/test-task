package com.test.task.data.services

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import com.google.firebase.auth.FirebaseAuth
import com.test.task.common.App
import com.test.task.data.maneger.CacheManager
import com.test.task.utils.extensions.toast

class ClearPreferenceService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        /**
         * this Handler to remove the admin auth info after 15 min
         */
        Handler().postDelayed({
            "Auth info removed".toast(context = App.instance)

            FirebaseAuth.getInstance().signOut()
            CacheManager.instance.clearAll()

        },900000)

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder = Binder()
}
