package com.test.task.data.di.annotations

import javax.inject.Qualifier

@Qualifier
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class DepartmentsAnno(val value: String = "departmentModel")
