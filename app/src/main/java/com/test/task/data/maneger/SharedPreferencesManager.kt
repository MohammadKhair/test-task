package com.test.task.data.maneger

import android.content.Context
import android.content.SharedPreferences
import com.test.task.common.App

/**
 * I'm using CacheManager and SharedPreferencesManager to control the saving data
 *
 */

class SharedPreferencesManager {

    //------------ Shared Preferences Constructor ----------\\
    init {
        initPreference()
    }

    fun clearPreference() {
        oEditor!!.clear()
        oEditor!!.apply()
    }

    //------------ Get Shared Preferences Value ------------\\
    fun getStringPreferences(Key: String, sDefaultValue: String): String? {
        return oSharedPreferences!!.getString(Key, sDefaultValue)
    }

    fun getLongPreferences(Key: String, sDefaultValue: Long): Long {
        return oSharedPreferences!!.getLong(Key, sDefaultValue)
    }

    fun getBooleanPreferences(Key: String, sDefaultValue: Boolean): Boolean {
        return oSharedPreferences!!.getBoolean(Key, sDefaultValue)
    }

    fun getIntPreferences(Key: String, sDefaultValue: Int): Int {
        return oSharedPreferences!!.getInt(Key, sDefaultValue)
    }

    fun getFloatPreferences(Key: String, sDefaultValue: Float): Float {
        return oSharedPreferences!!.getFloat(Key, sDefaultValue)
    }

    //------------ Set Shared Preferences Value ------------\\

    fun setStringPreferences(Key: String, sValue: String) {
        oEditor!!.putString(Key, sValue).apply()
    }

    fun setLongPreferences(Key: String, sValue: Long) {
        oEditor!!.putLong(Key, sValue).apply()
    }

    fun setBooleanPreferences(Key: String, sValue: Boolean) {
        oEditor!!.putBoolean(Key, sValue).apply()
    }


    fun setIntPreferences(Key: String, sValue: Int) {
        oEditor!!.putInt(Key, sValue).apply()
    }

    fun setFloatPreferences(Key: String, sValue: Float) {
        oEditor!!.putFloat(Key, sValue).apply()
    }

    companion object {
        private var oSharedPreferences: SharedPreferences? = null

        private var oEditor: SharedPreferences.Editor? = null

        private fun initPreference() {
            if (oSharedPreferences == null) {
                oSharedPreferences = App.instance.getSharedPreferences(
                    "test.task",
                    Context.MODE_PRIVATE
                )
            }

            if (oEditor == null) {
                oEditor = oSharedPreferences!!.edit()
                oEditor!!.apply()
            }
        }
    }
}
