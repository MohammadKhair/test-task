package com.test.task.data.di.annotations

import javax.inject.Qualifier

@Qualifier
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class EmployeesAnno(val value: String = "employeesModel")
