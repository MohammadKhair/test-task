package com.test.task.data.models

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.test.task.BR
import com.test.task.R
import com.test.task.common.App

class SignInModel(private var username: String, private var password: String) : BaseObservable() {

    @Bindable
    fun getUsername(): String {
        return username
    }

    @Bindable
    fun getPassword(): String {
        return password
    }

    @Bindable
    fun setUsername(username: String) {
        this.username = username
        notifyPropertyChanged(BR.username)
        notifyPropertyChanged(BR.usernameError)
    }

    @Bindable
    fun setPassword(password: String) {
        this.password = password
        notifyPropertyChanged(BR.password)
        notifyPropertyChanged(BR.passwordError)
    }

    @Bindable
    fun getUsernameError(): String? {
        return if (username.isEmpty()) {
            App.instance.getString(R.string.username_error)
        } else {
            null
        }
    }

    @Bindable
    fun getPasswordError(): String? {
        return if (password.isEmpty()) {
            App.instance.getString(R.string.password_error)
        } else {
            null
        }
    }
}
