package com.test.task.data.models

import android.os.Parcelable
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bumptech.glide.Glide
import com.test.task.BR
import com.test.task.R
import com.test.task.common.App
import com.test.task.data.models.EmployeesModel.Companion.tableName
import com.test.task.utils.extensions.random
import com.test.task.utils.extensions.stringToBitMap
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.employee_item.view.*

/**
 * I'm using BaseObservable to notify the changing in the variable while using the model in data binding in xml or anywhere else
 * and using Parcelize to pass the model in ui navigation component as an argument
 *
 * and I'm preparing this model for the rooming
 */

@Parcelize
@Entity(tableName = tableName)
data class EmployeesModel(
    @PrimaryKey
    @ColumnInfo(name = employeeId)
//    @SerializedName("id")
//    @Expose
    private var id: Int,
    @ColumnInfo(name = employeeDepartmentId)
    private var departmentId: Int,
    @ColumnInfo(name = employeeDepartmentName)
    private var departmentName: String,
    @ColumnInfo(name = employeeCreatedDate)
    private var createdDate: String,
    @ColumnInfo(name = employeeUpdateDate)
    private var updateDate: String,
    @ColumnInfo(name = employeeFirstName)
    private var firstName: String,
    @ColumnInfo(name = employeeLastName)
    private var lastName: String,
    @ColumnInfo(name = employeeEmail)
    private var email: String,
    @ColumnInfo(name = employeeMobileNumber)
    private var mobileNumber: String,
    @ColumnInfo(name = employeePassword)
    private var password: String,
    @ColumnInfo(name = employeeGender)
    private var gender: String,
    @ColumnInfo(name = employeeAddress)
    private var address: String,
    @ColumnInfo(name = employeePhoto)
    private var photo: String
) : BaseObservable(), Parcelable {

    @Bindable
    fun getId(): Int {
        return id
    }

    @Bindable
    fun getDepartmentId(): Int {
        return departmentId
    }

    @Bindable
    fun getDepartmentName(): String {
        return departmentName
    }

    @Bindable
    fun getCreatedDate(): String {
        return createdDate
    }

    @Bindable
    fun getUpdateDate(): String {
        return updateDate
    }

    @Bindable
    fun getFirstName(): String {
        return firstName
    }

    @Bindable
    fun getLastName(): String {
        return lastName
    }

    @Bindable
    fun getEmail(): String {
        return email
    }

    @Bindable
    fun getMobileNumber(): String {
        return mobileNumber
    }

    @Bindable
    fun getGender(): String {
        return gender
    }

    @Bindable
    fun getAddress(): String {
        return address
    }

    @Bindable
    fun getPassword(): String {
        return password
    }

    @Bindable
    fun getPhoto(): String {
        return photo
    }

    @Bindable
    fun setId(id: Int) {
        this.id = id
        notifyPropertyChanged(BR.id)
    }

    @Bindable
    fun setDepartmentId(departmentId: Int) {
        this.departmentId = departmentId
        notifyPropertyChanged(BR.departmentId)
    }

    @Bindable
    fun setDepartmentName(departmentName: String) {
        this.departmentName = departmentName
        notifyPropertyChanged(BR.departmentName)
    }

    @Bindable
    fun setCreatedDate(createdDate: String) {
        this.createdDate = createdDate
        notifyPropertyChanged(BR.createdDate)
    }

    @Bindable
    fun setUpdateDate(updateDate: String) {
        this.updateDate = updateDate
        notifyPropertyChanged(BR.updateDate)
    }

    @Bindable
    fun setFirstName(firstName: String) {
        this.firstName = firstName
        notifyPropertyChanged(BR.firstName)
        notifyPropertyChanged(BR.firstNameError)
    }

    @Bindable
    fun setLastName(lastName: String) {
        this.lastName = lastName
        notifyPropertyChanged(BR.lastName)
        notifyPropertyChanged(BR.lastNameError)
    }

    @Bindable
    fun setEmail(email: String) {
        this.email = email
        notifyPropertyChanged(BR.email)
        notifyPropertyChanged(BR.emailError)
    }

    @Bindable
    fun setMobileNumber(mobileNumber: String) {
        this.mobileNumber = mobileNumber
        notifyPropertyChanged(BR.mobileNumber)
        notifyPropertyChanged(BR.mobileNumberError)
    }

    @Bindable
    fun setGender(gender: String) {
        this.gender = gender
        notifyPropertyChanged(BR.gender)
        notifyPropertyChanged(BR.genderError)
    }

    @Bindable
    fun setAddress(address: String) {
        this.address = address
        notifyPropertyChanged(BR.address)
        notifyPropertyChanged(BR.addressError)
    }

    @Bindable
    fun setPassword(password: String) {
        this.password = password
        notifyPropertyChanged(BR.password)
        notifyPropertyChanged(BR.passwordError)
    }

    @Bindable
    fun setPhoto(photo: String) {
        this.photo = photo
        notifyPropertyChanged(BR.photo)
    }

    @Bindable
    fun getFirstNameError(): String? {
        return if (firstName.isEmpty()) {
            App.instance.getString(R.string.first_name_error)
        } else {
            null
        }
    }

    @Bindable
    fun getLastNameError(): String? {
        return if (lastName.isEmpty()) {
            App.instance.getString(R.string.last_name_error)
        } else {
            null
        }
    }

    @Bindable
    fun getEmailError(): String? {
        return if (email.isEmpty()) {
            App.instance.getString(R.string.email_error)
        } else {
            null
        }
    }

    @Bindable
    fun getMobileNumberError(): String? {
        return if (mobileNumber.isEmpty()) {
            App.instance.getString(R.string.mobile_number_error)
        } else {
            null
        }
    }

    @Bindable
    fun getGenderError(): String? {
        return if (gender.isEmpty()) {
            App.instance.getString(R.string.gender_error)
        } else {
            null
        }
    }

    @Bindable
    fun getAddressError(): String? {
        return if (address.isEmpty()) {
            App.instance.getString(R.string.address_error)
        } else {
            null
        }
    }

    @Bindable
    fun getPasswordError(): String? {
        return if (password.isEmpty()) {
            App.instance.getString(R.string.password_error)
        } else {
            null
        }
    }

    companion object {
        @BindingAdapter("loadEmployeeImage")
        @JvmStatic
        fun loadImage(view: View, model: EmployeesModel) {
            if (model.getPhoto().isNotEmpty()) {
                Glide.with(view.context)
                    .load(stringToBitMap(model.getPhoto()))
                    .into(view.profile_image)

            } else {
                /**
                 * I'm using this way to add an image to the imageView if the employee model doesn't have a photo depend on the gender
                 */

                if (model.gender== "Male") {

                    when (random(start = 1, end = 3)) {
                        1 -> Glide.with(view.context).load(R.drawable.ic_person_1).into(view.profile_image)
                        2 -> Glide.with(view.context).load(R.drawable.ic_person_3).into(view.profile_image)
                        3 -> Glide.with(view.context).load(R.drawable.ic_person_6).into(view.profile_image)
                    }

                } else {
                    when (random(start = 1, end = 3)) {
                        1 -> Glide.with(view.context).load(R.drawable.ic_person_2).into(view.profile_image)
                        2 -> Glide.with(view.context).load(R.drawable.ic_person_4).into(view.profile_image)
                        3 -> Glide.with(view.context).load(R.drawable.ic_person_5).into(view.profile_image)
                    }
                }
            }
        }

        const val tableName="employees"
        const val employeeId = "id"
        const val employeeDepartmentId ="departmentId"
        const val employeeDepartmentName = "name"
        const val employeeCreatedDate = "createdDate"
        const val employeeUpdateDate = "updateDate"
        const val employeeFirstName = "firstName"
        const val employeeLastName = "lastName"
        const val employeeEmail = "email"
        const val employeeMobileNumber = "mobileNumber"
        const val employeePassword = "password"
        const val employeeGender = "gender"
        const val employeeAddress = "address"
        const val employeePhoto = "photo"
    }
}
