package com.test.task.data.repositories

import com.test.task.common.App
import com.test.task.data.db.DataBase
import com.test.task.data.models.DepartmentModel
import com.test.task.data.models.EmployeesModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * this repository is the responsible for hit on the apis and take the data and save it in local database
 * but there is no online apis now so it's just handling with local database (roomDB)
 *
 */

class EmployeesRepository @Inject constructor() {
    fun getLocalDepartment(): Observable<List<DepartmentModel>> =
        DataBase.getDatabaseInstance(App.instance).departmentDao()
            .getDepartments()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun insertLocalEmployee(model: EmployeesModel): Completable =
        DataBase.getDatabaseInstance(App.instance).employeesDao()
            .insertEmployees(employee = model)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun updateLocalEmployeeImage(model: EmployeesModel): Completable =
        DataBase.getDatabaseInstance(App.instance).employeesDao()
            .updateEmployeeImage(model = model)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun updateLocalEmployee(model: EmployeesModel): Completable =
        DataBase.getDatabaseInstance(App.instance).employeesDao()
            .updateLocalEmployee(model = model)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

}
