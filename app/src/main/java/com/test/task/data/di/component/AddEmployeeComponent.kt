package com.test.task.data.di.component

import com.test.task.data.di.annotations.DepartmentsAnno
import com.test.task.data.di.scope.AddEmployeeScope
import com.test.task.data.models.DepartmentModel
import com.test.task.ui.employee.add.AddEmployee
import dagger.BindsInstance
import dagger.Subcomponent

/**
 * this is the the second sub component which injected in AppComponent
 *
 * I'm injecting AddEmployee and passing DepartmentModel DepartmentsAnno annotation with to use it with DepartmentsAdapter adapter before building injection
 *
 * we using the DepartmentsAnno annotation to tell the app take a specific data
 */

@AddEmployeeScope
@Subcomponent
interface AddEmployeeComponent {

    fun injectFragment(addEmployee: AddEmployee)

    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun list(@DepartmentsAnno model: List<DepartmentModel>) : Builder

        fun build() : AddEmployeeComponent
    }
}
