package com.test.task.data.network

import com.test.task.data.models.EmployeesModel
import io.reactivex.Observable
import retrofit2.http.POST
import javax.inject.Singleton

/**
 * this class is the responsible for the endpoints
 *
 */

@Singleton
interface EndpointInterface {

    @POST("getEmployees.php")
    fun getEmployees(
    ): Observable<List<EmployeesModel>>

}
