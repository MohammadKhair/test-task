package com.test.task.data.di.component

import androidx.navigation.NavController
import com.test.task.data.di.annotations.EmployeesAnno
import com.test.task.data.di.scope.HomeScope
import com.test.task.data.models.EmployeesModel
import com.test.task.ui.home.HomeFragment
import dagger.BindsInstance
import dagger.Subcomponent

/**
 * this is the the second sub component which injected in AppComponent
 *
 * I'm injecting AddEmployee and passing navController and EmployeesModel EmployeesAnno annotation with to use it with HomeAdapter adapter before building injection
 *
 * we using the EmployeesAnno annotation to tell the app take a specific data
 */

@HomeScope
@Subcomponent
interface HomeComponent {

    fun injectHomeFragment(homeFragment: HomeFragment)

    @Subcomponent.Builder
    interface Builder {
        @BindsInstance fun list(@EmployeesAnno model: List<EmployeesModel>) : Builder
        @BindsInstance fun navController(navController: NavController) : Builder

        fun build() : HomeComponent
    }
}
