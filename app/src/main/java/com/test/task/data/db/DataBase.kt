package com.test.task.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.test.task.data.dao.DepartmentDao
import com.test.task.data.dao.EmployeesDao
import com.test.task.data.models.DepartmentModel
import com.test.task.data.models.EmployeesModel

/**
 * this class using to setup the database
 */

@Database(entities = [EmployeesModel::class, DepartmentModel::class], version = 6)
abstract class DataBase : RoomDatabase() {

    abstract fun employeesDao(): EmployeesDao
    abstract fun departmentDao(): DepartmentDao

    companion object {
        @Volatile
        private var databaseInstance: DataBase? = null

        fun getDatabaseInstance(mContext: Context): DataBase =
            databaseInstance ?: synchronized(this) {
                databaseInstance ?: buildDatabaseInstance(mContext).also {
                    databaseInstance = it
                }
            }

        private fun buildDatabaseInstance(mContext: Context) =
            Room.databaseBuilder(mContext, DataBase::class.java, "task")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()

    }
}
