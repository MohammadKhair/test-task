package com.test.task.data.di.component

import com.test.task.common.App
import com.test.task.data.di.modules.ContextModules
import com.test.task.ui.department.DepartmentViewModel
import com.test.task.ui.employee.EmployeeViewModel
import com.test.task.ui.home.HomeViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * this is the main component in dependency injection using dagger
 * I'm injecting two sub component
 * 1. HomeComponent
 * 2. AddEmployeeComponent
 * 3. EditEmployeeComponent
 *
 * I'm injecting App, HomeViewModel, DepartmentViewModel, EmployeeViewModel and EditEmployeeComponent
 *
 * and using the Singleton scope to make it static in all the app
 */

@Singleton
@Component(modules = [ContextModules::class])
interface AppComponent {
    val app: App

    val getHomeComponentBuilder: HomeComponent.Builder
    val getAddEmployeeComponentBuilder: AddEmployeeComponent.Builder
    val getEditEmployeeComponentBuilder: EditEmployeeComponent.Builder

    fun injectApp(app: App)
    fun injectHomeViewModel(homeViewModel: HomeViewModel)
    fun injectDepartmentViewModel(viewModel: DepartmentViewModel)
    fun injectEmployeeViewModel(viewModel: EmployeeViewModel)

    @Component.Builder
    interface Builder {

        fun build() : AppComponent
    }
}
