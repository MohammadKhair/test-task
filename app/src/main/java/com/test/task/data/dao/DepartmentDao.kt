package com.test.task.data.dao

import androidx.room.*
import com.test.task.data.models.DepartmentModel
import io.reactivex.Completable
import io.reactivex.Observable

/**
 * this Dao to control the some func in department table in the SQLite database
 */

@Dao
interface DepartmentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDepartment(department: DepartmentModel) : Completable

//    @Query("UPDATE  ${DepartmentModel.tableName} SET ${DepartmentModel.departmentName}=:name, ${DepartmentModel.departmentUpdateDate}=:updateDate WHERE ${DepartmentModel.departmentId} = :id")
//    fun updateDepartment(id: Int, name: String, updateDate: String) : Completable

    @Query("SELECT * FROM ${DepartmentModel.tableName}")
    fun getDepartments(): Observable<List<DepartmentModel>>

    @Update
    fun updateDepartment(model: DepartmentModel) : Completable
}
