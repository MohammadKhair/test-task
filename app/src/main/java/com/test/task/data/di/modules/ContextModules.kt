package com.test.task.data.di.modules

import android.content.Context
import com.test.task.common.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * this Module is using to provide the context in dagger to use it without passing context to classes
 *
 */

@Module
class ContextModules {

    @Singleton
    @Provides
    fun providesContext(): Context {
        return App.instance.applicationContext
    }
}
