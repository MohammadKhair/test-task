package com.test.task.data.repositories

import com.test.task.common.App
import com.test.task.data.db.DataBase
import com.test.task.data.models.EmployeesModel
import com.test.task.utils.NetManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * this repository is the responsible for hit on the apis and take the data and save it in local database
 * but there is no online apis now so it's just handling with local database (roomDB)
 *
 */

class HomeRepository @Inject constructor(private val netManager: NetManager) {
//    fun getEmployees(): Observable<List<EmployeesModel>> {
//        netManager.isConnectedToInternet?.let {
//            if (it) {
//                return getGlobalEmployees().flatMap { it2 ->
//                    return@flatMap saveLocalEmployees(it2)
//                        .toSingleDefault(it2)
//                        .toObservable()
//                }
//            }
//        }
//
//        return Observable.just(getLocalEmployees())
//    }

    fun getEmployees(): Observable<List<EmployeesModel>> = getLocalEmployees()

//    private fun getGlobalEmployees() : Observable<List<EmployeesModel>> =
//        App.instance.getApis().getEmployees()
//            .subscribeOn(Schedulers.computation())
//            .observeOn(AndroidSchedulers.mainThread())

    private fun getLocalEmployees() : Observable<List<EmployeesModel>> =
        DataBase.getDatabaseInstance(App.instance)
            .employeesDao().getEmployees()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

//    private fun saveLocalEmployees(employees: List<EmployeesModel>): Completable =
//        DataBase.getDatabaseInstance(App.instance).employeesDao().insertEmployees(employees)

}
