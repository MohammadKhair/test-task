package com.test.task.data.maneger

/**
 * I'm using CacheManager and SharedPreferencesManager to control the saving data
 *
 */

class CacheManager private constructor() {
    private val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }
    private var username: String = ""
    private var password: String = ""

    init {
        username = mSharedPreference.getStringPreferences("username", "")!!
        password = mSharedPreference.getStringPreferences("password", "")!!

    }

    fun clearAll() {
        mSharedPreference.clearPreference()
        mInstance = CacheManager()
        username = ""
        password = ""
    }

    //------------ Get Cache Value ------------//

    fun getUsername(): String {
        return username
    }

    fun getPassword(): String {
        return password
    }

    //------------ Set Cache Value ------------//

    fun setUsername(username: String) {
        this.username = username
        mSharedPreference.setStringPreferences("username", username)
    }

    fun setPassword(password: String) {
        this.password = password
        mSharedPreference.setStringPreferences("password", password)
    }

    companion object {
        private var mInstance: CacheManager? = null

        val instance: CacheManager
            @Synchronized get() {
                if (mInstance == null) {
                    mInstance = CacheManager()
                }
                return mInstance!!
            }
    }
}