package com.test.task.data.models

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.test.task.BR
import com.test.task.R
import com.test.task.common.App
import com.test.task.data.models.DepartmentModel.Companion.tableName
import kotlinx.android.parcel.Parcelize

/**
 * I'm using BaseObservable to notify the changing in the variable while using the model in xml or anywhere
 * and using Parcelize to pass the model in ui navigation component as an argument
 *
 * and I'm preparing this model for the rooming
 */

@Parcelize
@Entity(tableName = tableName)
data class DepartmentModel(
    @PrimaryKey
    @ColumnInfo(name = departmentId)
    private var id: Int,
    @ColumnInfo(name = departmentName)
    private var name: String,
    @ColumnInfo(name = departmentCreatedDate)
    private var createdDate: String,
    @ColumnInfo(name = departmentUpdateDate)
    private var updateDate: String)
    : BaseObservable(), Parcelable {

    @Bindable
    fun getId(): Int {
        return id
    }

    @Bindable
    fun getName(): String {
        return name
    }

    @Bindable
    fun getCreatedDate(): String {
        return createdDate
    }

    @Bindable
    fun getUpdateDate(): String {
        return updateDate
    }

    @Bindable
    fun setId(id: Int) {
        this.id = id
        notifyPropertyChanged(BR.id)
    }

    @Bindable
    fun setName(name: String) {
        this.name = name
        notifyPropertyChanged(BR.name)
        notifyPropertyChanged(BR.nameError)
    }

    @Bindable
    fun setCreatedDate(createdDate: String) {
        this.createdDate = createdDate
        notifyPropertyChanged(BR.createdDate)
    }

    @Bindable
    fun setUpdateDate(updateDate: String) {
        this.updateDate = updateDate
        notifyPropertyChanged(BR.updateDate)
    }

    @Bindable
    fun getNameError(): String? {
        return if (name.isEmpty()) {
            App.instance.getString(R.string.department_name_error)
        } else {
            null
        }
    }

    companion object {

        const val tableName="departments"
        const val departmentId = "id"
        const val departmentName = "name"
        const val departmentCreatedDate = "departmentCreatedDate"
        const val departmentUpdateDate = "departmentUpdateDate"
    }
}
