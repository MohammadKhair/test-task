package com.test.task.common

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.test.task.data.di.component.AppComponent
import com.test.task.data.di.component.DaggerAppComponent
import com.test.task.data.network.Api
import com.test.task.utils.LocaleHelper
import io.paperdb.Paper
import java.util.*
import javax.inject.Inject

/**
 * I'm daggering the AppComponent to make a singleton from some classes like Api and other classes
 * but I have no OkHttp request so I did'nt use it ^_^
 *
 */

class App @Inject constructor(): Application() {

    @Inject lateinit var api: Api
    private lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()
        instance = this

        component= DaggerAppComponent.builder().build()
        component.injectApp(app = this)

        Paper.init(this)
        settingMultipleLanguages()

    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"))
        MultiDex.install(this)
    }

    private fun settingMultipleLanguages() {
        val firstLaunchLocale = Locale("en")
        val arabic = Locale("ar")
        val supportedLocales = HashSet<Locale>()
        supportedLocales.add(firstLaunchLocale)
        supportedLocales.add(arabic)
    }

    fun getComponent() : AppComponent = component

    companion object {
        lateinit var instance: App
            private set
    }
}
