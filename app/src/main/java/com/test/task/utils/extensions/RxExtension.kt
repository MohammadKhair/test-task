package com.test.task.utils.extensions

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * this extension use += instead of add in compositeDisposable
 *
 */

operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    add(disposable)
}