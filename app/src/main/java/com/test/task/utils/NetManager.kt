package com.test.task.utils

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject

/**
 * this class to chack from the internet connection
 *
 */

class NetManager @Inject constructor(private var applicationContext: Context) {
    val isConnectedToInternet: Boolean?
        get() {
            val conManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val ni = conManager.activeNetworkInfo
            return ni != null && ni.isConnected
        }
}
