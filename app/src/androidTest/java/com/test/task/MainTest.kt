package com.test.task

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiObjectNotFoundException
import com.test.task.ui.splash.SplashActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainTest {
    @get:Rule
    var splashActivityTestRule: ActivityTestRule<SplashActivity> = ActivityTestRule(
        SplashActivity::class.java
    )

//    @get:Rule
//    var mainActivityTestRule: ActivityTestRule<MainActivity> = ActivityTestRule(
//        MainActivity::class.java
//    )

    @Test
    @Throws(UiObjectNotFoundException::class, InterruptedException::class)
    fun testUiAutomatorAPI() {

        Thread.sleep(3000)
        //enter username
        Espresso.onView(withId(R.id.username_auth)).perform(ViewActions.typeText("admin"))
        Espresso.onView(withId(R.id.username_auth)).perform(closeSoftKeyboard())

        Thread.sleep(1000)
        //enter Password
        Espresso.onView(withId(R.id.password_auth)).perform(ViewActions.typeText("123456"))
        Espresso.onView(withId(R.id.password_auth)).perform(closeSoftKeyboard())

        Thread.sleep(1000)
        //press signIn
        Espresso.onView(withId(R.id.signIn)).perform(click())

//        Thread.sleep(1000)
//        //press settings
//        Espresso.onView(withId(R.id.settings)).perform(click())


    }
}